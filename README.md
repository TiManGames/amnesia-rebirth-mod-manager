# Amnesia: Rebirth Mod Manager

The Amnesia: Rebirth Mod Manager is a JavaFX Spring-Boot application, which makes it easier to manage and configure mods for the 
game Amnesia: Rebirth, developed by Frictional Games. No need to mess with XML and CFG files anymore.
Everything is in one place and can easily be changed.

Features:
- **Add or create new mod entries for easy management.**

- **Edit mod information: Name, Author, Description, Thumbnail Photo and Mod Type.**

- **Manage mod dependencies with ease**.

- **Sync your mod to the Level Editor: If your mod has any custom assets, the level editor will automatically load it 
after it's synced.**

- **Create a custom .dev launch file that will load your mod with your custom scripts and assets.**

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes.

### Prerequisites

- You must own the game Amnesia: Rebirth in order to fully test the application.
- Jdk 1.8.0_251
- Maven 3.6.0+
- SceneBuilder for GUI development

**You must get familiar with JavaFX and Spring-Boot if you want to contribute this project.**

### Installing

- Clone this project and download the Maven dependencies.
- Install SceneBuilder if you want to modify the application's GUI.

### Running the application

The main class of the application is `ModManagerApplication.java` which uses `SpringApplicationContext` to launch itself.


In order to reduce Spring initialization and startup times, add to your run configuration the following VM Options:
 
 `-XX:TieredStopAtLevel=1 -Dspring.jmx.enabled=false -noverify`

### Running the tests

This project uses JUnit5 and TestFX for testing. The main test class is `RegressionTests.java`. While other test classes
can be created, this class  should run no matter what when building the project with Maven.
Make sure your test class extends `TestFXBase`.

Currently, the application testing isn't headless, so you must wait and let the test run without interference.

### Building the project

Use Maven to build the project via `mvn clean install`. 
A runnable jar should be produced inside the project build folder.
