package org.fg.rebirth.manager.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import org.fg.rebirth.manager.models.ViewDataModel;
import org.fg.rebirth.manager.models.config.ApplicationConfig;
import org.fg.rebirth.manager.models.mod.Mod;
import org.fg.rebirth.manager.models.mod.ModProfilesManager;
import org.fg.rebirth.manager.util.XmlDocument;
import org.fg.rebirth.manager.util.application.DialogConstants;
import org.fg.rebirth.manager.util.application.SelectorsConstants;
import org.fg.rebirth.manager.util.javafx.AlertMaker;
import org.fg.rebirth.manager.util.javafx.FileSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModProfilesController implements Initializable {
    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @Autowired
    private ViewDataModel viewDataModel;

    /////////////////////
    // BUTTONS
    ////////////////////

    @FXML
    public Button addModBtn;

    @FXML
    public Button newModBtn;

    @FXML
    public Button removeModBtn;

    /////////////////////
    // TABLES
    ////////////////////

    @FXML
    private TableView<Mod> modProfilesTable;

    @FXML
    private TableColumn<Mod, SimpleStringProperty> modNameCol;

    @FXML
    private TableColumn<Mod, SimpleStringProperty> modTypeCol;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Tables
        modProfilesTable.setRowFactory(fac -> {
            TableRow<Mod> row = new TableRow<>();
            row.setOnMouseClicked(this::onClickModProfileTableRow);

            return row;
        });

        modProfilesTable.itemsProperty().bindBidirectional(viewDataModel.modProfilesProperty());
        modProfilesTable.disableProperty().bind(viewDataModel.gamePathProperty().isEmpty());
        modProfilesTable.setItems(modProfilesManager.getModProfiles());

        viewDataModel.bindModProfilesTable(modProfilesTable);

        modNameCol.setCellValueFactory(new PropertyValueFactory<>("modName"));
        modTypeCol.setCellValueFactory(new PropertyValueFactory<>("modType"));

        // Data model
        viewDataModel.modProfilesDisableProperty().bind(modProfilesTable.disabledProperty());

        // Buttons
        addModBtn.disableProperty().bind(modProfilesTable.disabledProperty());
        newModBtn.disableProperty().bind(modProfilesTable.disabledProperty());
        removeModBtn.disableProperty().bind(
                Bindings.or(
                        modProfilesTable.disabledProperty(),
                        modProfilesTable.selectionModelProperty().get().selectedItemProperty().isNull()
                )
        );
    }

    @FXML
    private void onClickModProfileTableRow(MouseEvent mouseEvent) {
        Mod selectedMod = getSelectedMod();

        if (selectedMod != null) {
            modProfilesManager.setSelectedMod(selectedMod);

            viewDataModel.modNameProperty().setValue(selectedMod.getModName());
            viewDataModel.modAuthorProperty().setValue(selectedMod.getModAuthor());
            viewDataModel.modDescriptionProperty().setValue(selectedMod.getModDescription());
            viewDataModel.modThumbnailProperty().setValue(selectedMod.getModImageName());
            viewDataModel.modTypeProperty().setValue(selectedMod.getModType().getTypeName());
        }
    }

    @FXML
    private void onClickRemoveModBtn() {
        Mod selectedMod = getSelectedMod();

        modProfilesManager.removeMod(selectedMod);
        modProfilesTable.getItems().remove(selectedMod);

        viewDataModel.modNameProperty().setValue("");
        viewDataModel.modAuthorProperty().setValue("");
        viewDataModel.modDescriptionProperty().setValue("");
        viewDataModel.modThumbnailProperty().setValue("");
        viewDataModel.modTypeProperty().setValue("Mod Type");
    }

    @FXML
    private void onClickAddModBtn() {
        File modEntryFile = FileSelector.selectFile(SelectorsConstants.SELECT_MOD_TITLE,
                applicationConfig.getGameFolderPath(),
                new FileChooser.ExtensionFilter(SelectorsConstants.SELECT_MOD_DESCRIPTION, "*.hpc"));

        if (modEntryFile != null) {
            Mod mod = new Mod(modEntryFile.toPath());

            if (modProfilesManager.addMod(mod)) {
                modProfilesTable.setItems(modProfilesManager.getModProfiles());
            } else {
                AlertMaker.info(DialogConstants.DIALOG_MOD_ADD_INFO_TITLE,
                        DialogConstants.DIALOG_MOD_ADD_INFO_HEADER,
                        DialogConstants.DIALOG_MOD_ADD_INFO_CONTENT);
            }
        }
    }

    @FXML
    private void onClickNewModBtn() {
        StringBuilder nameBuilder = new StringBuilder("New mod");
        ObservableList<Mod> items = modProfilesManager.getModProfiles();

        for (Mod mod : items) {
            if (mod.getModName().equals("New mod")) {
                nameBuilder.append("_").append(modProfilesManager.getModProfiles().size());

                break;
            }
        }

        Mod newMod = new Mod(nameBuilder.toString(),
                "Author",
                "Description",
                applicationConfig.getGameFolderPath());

        modProfilesManager.addMod(newMod);
        modProfilesTable.setItems(modProfilesManager.getModProfiles());

        // Update .hpc file
        XmlDocument modEntry;

        File modFolder = newMod.getLocation().toFile();

        if (!modFolder.getParentFile().exists()) {
            if (modFolder.getParentFile().mkdir()) {
                modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));

                modEntry.setRootElement("Content");
                modEntry.setRootAttribute("Version", "1.0.0");
                modEntry.setRootAttribute("InitCfg", "config/main_init.cfg");
            } else {
                AlertMaker.error(DialogConstants.DIALOG_MOD_APPLY_ERROR_TITLE,
                        DialogConstants.DIALOG_MOD_APPLY_ERROR_HEADER,
                        DialogConstants.DIALOG_MOD_APPLY_ERROR_CONTENT);

                return;
            }
        } else {
            modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));
        }

        modEntry.setRootAttribute("Type", newMod.getModType().getTypeName());
        modEntry.setRootAttribute("Title", newMod.getModName());
        modEntry.setRootAttribute("Author", newMod.getModAuthor());
        modEntry.setRootAttribute("Description", newMod.getModDescription());
        modEntry.setRootAttribute("LauncherPic", newMod.getModImageName());
        modEntry.setRootAttribute("UID", newMod.getUID());
        modEntry.setRootAttribute("Dependencies", newMod.getDependencyNames());
        modEntry.save();
    }

    private Mod getSelectedMod() {
        return modProfilesTable.getSelectionModel().getSelectedItem();
    }
}
