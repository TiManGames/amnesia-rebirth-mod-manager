package org.fg.rebirth.manager.controllers;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import org.fg.rebirth.manager.config.ApplicationTheme;
import org.fg.rebirth.manager.config.StageManager;
import org.fg.rebirth.manager.models.config.ApplicationConfig;
import org.fg.rebirth.manager.util.application.DialogConstants;
import org.fg.rebirth.manager.util.application.GlobalConstants;
import org.fg.rebirth.manager.util.javafx.AlertMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    @Lazy
    private StageManager stageManager;

    @Autowired
    private ApplicationConfig applicationConfig;

    @FXML
    private void onClickAbout() {
        AlertMaker.info(DialogConstants.DIALOG_ABOUT_TITLE,
                DialogConstants.DIALOG_ABOUT_HEADER,
                DialogConstants.DIALOG_ABOUT_CONTENT);
    }

    @FXML
    private void onClickViewRepository() {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI(GlobalConstants.APPLICATION_REPOSITORY_URL));
            } catch (IOException | URISyntaxException e) {
                LOGGER.error(e.getMessage());
            }
        } else {
            AlertMaker.error(DialogConstants.DIALOG_REPO_TITLE,
                    DialogConstants.DIALOG_REPO_HEADER,
                    DialogConstants.DIALOG_REPO_CONTENT);
        }
    }

    @FXML
    private void onClickColorThemeOption(Event event) {
        MenuItem item = (MenuItem) event.getSource();
        ApplicationTheme theme = Enum.valueOf(ApplicationTheme.class, item.getText().toUpperCase());

        stageManager.setTheme(theme);
        applicationConfig.setTheme(theme);
    }
}
