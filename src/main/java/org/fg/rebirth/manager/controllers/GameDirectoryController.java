package org.fg.rebirth.manager.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import org.fg.rebirth.manager.models.ViewDataModel;
import org.fg.rebirth.manager.models.config.ApplicationConfig;
import org.fg.rebirth.manager.models.mod.Mod;
import org.fg.rebirth.manager.models.mod.ModProfilesManager;
import org.fg.rebirth.manager.util.application.GlobalConstants;
import org.fg.rebirth.manager.util.application.SelectorsConstants;
import org.fg.rebirth.manager.util.javafx.DirectorySelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GameDirectoryController implements Initializable {
    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @Autowired
    private ViewDataModel viewDataModel;

    @FXML
    private TextField gamePathField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gamePathField.textProperty().bindBidirectional(viewDataModel.gamePathProperty());

        String pathToSet;
        if (GlobalConstants.USER_DIRECTORY.contains("Amnesia Rebirth")) {
            applicationConfig.setGameFolderPath(GlobalConstants.USER_DIRECTORY);

            pathToSet = GlobalConstants.USER_DIRECTORY;

            File modsFolder = new File("mods" + File.separator);
            if (modsFolder.exists()) {
                String[] modFolders = modsFolder.list();

                if (modFolders != null && modFolders.length > 0) {
                    Arrays.stream(modFolders)
                            .map(folder -> new File(modsFolder.getAbsolutePath() + File.separator + folder + File.separator + "entry.hpc"))
                            .forEach(modFile -> modProfilesManager.addMod(new Mod(modFile.toPath())));
                }
            }
        } else {
            pathToSet = applicationConfig.getGameFolderPath();
        }

        gamePathField.setText(pathToSet);
    }

    @FXML
    private void onClickChooseGameFolderBtn() {
        File directory = DirectorySelector.selectDirectory(SelectorsConstants.SELECT_GAME_DIRECTORY_TITLE);
        String pathName = (directory != null ? directory.getAbsolutePath() : "");

        applicationConfig.setGameFolderPath(pathName);
        gamePathField.setText(pathName);
    }
}
