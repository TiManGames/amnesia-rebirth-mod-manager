package org.fg.rebirth.manager.controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import org.fg.rebirth.manager.config.ApplicationView;
import org.fg.rebirth.manager.config.StageManager;
import org.fg.rebirth.manager.models.mod.Mod;
import org.fg.rebirth.manager.models.mod.ModDependency;
import org.fg.rebirth.manager.models.mod.ModProfilesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModDependenciesController implements Initializable {
    private Mod mod;

    @Lazy
    @Autowired
    private StageManager stageManager;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @FXML
    private TextField uidField;

    @FXML
    private TableView<ModDependency> dependenciesTable;

    @FXML
    private TableColumn<ModDependency, String> dependenciesCol;

    private void loadDependenciesFromMod(Mod mod) {
        String uid = mod.getUID();
        if (uid != null) {
            uidField.setText(uid);
        }

        // Dependencies Table
        List<ModDependency> dependencies = mod.getDependencies();

        if (!dependencies.isEmpty()) {
            dependenciesTable.setItems(FXCollections.observableList(dependencies));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dependenciesCol.setCellValueFactory(new PropertyValueFactory<>("DependencyName"));
        dependenciesCol.setCellFactory(TextFieldTableCell.forTableColumn());

        this.mod = modProfilesManager.getSelectedMod();
        loadDependenciesFromMod(mod);
    }

    @FXML
    private void onEditCommitDependenciesCol(CellEditEvent<ModDependency, String> modDependencyCellEdit) {
        ModDependency modDependency = dependenciesTable.getSelectionModel().getSelectedItem();
        modDependency.setDependencyName(modDependencyCellEdit.getNewValue());
    }

    @FXML
    private void onClickSaveDependencyBtn() {
        // Save dependencies
        dependenciesTable.getItems()
                .stream()
                .filter(dependency -> !mod.getDependencies().contains(dependency))
                .forEach(mod::addDependency);

        // Save UID
        mod.setUID(uidField.getText());

        stageManager.closeView(ApplicationView.MOD_DEPENDENCIES);
    }

    @FXML
    private void onClickRemoveDependencyBtn() {
        ModDependency selectedItem = dependenciesTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            dependenciesTable.getItems().remove(selectedItem);
        }
    }

    @FXML
    private void onClickAddDependencyBtnBtn() {
        dependenciesTable.getItems().add(new ModDependency("new.dependency"));
    }
}
