package org.fg.rebirth.manager.controllers;

import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import org.fg.rebirth.manager.config.ApplicationView;
import org.fg.rebirth.manager.config.StageManager;
import org.fg.rebirth.manager.models.ViewDataModel;
import org.fg.rebirth.manager.models.config.ApplicationConfig;
import org.fg.rebirth.manager.models.mod.Mod;
import org.fg.rebirth.manager.models.mod.ModProfilesManager;
import org.fg.rebirth.manager.models.mod.ModType;
import org.fg.rebirth.manager.util.EditorSync;
import org.fg.rebirth.manager.util.XmlDocument;
import org.fg.rebirth.manager.util.application.DialogConstants;
import org.fg.rebirth.manager.util.application.SelectorsConstants;
import org.fg.rebirth.manager.util.javafx.AlertMaker;
import org.fg.rebirth.manager.util.javafx.FileSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModInformationController implements Initializable {
    @Lazy
    @Autowired
    private StageManager stageManager;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @Autowired
    private ViewDataModel viewDataModel;

    @FXML
    private AnchorPane modInfoBottomPane;

    @FXML
    private GridPane modInfoGrid;

    /////////////////////
    // FIELDS
    ////////////////////

    @FXML
    private TextField modInfoNameField;

    @FXML
    private TextField modInfoAuthorField;

    @FXML
    private TextArea modDescriptionArea;

    @FXML
    private TextField modThumbnailField;

    /////////////////////
    // BUTTONS
    ////////////////////

    @FXML
    private MenuButton modTypeBtn;

    @FXML
    private Button applyBtn;

    @FXML
    private Button syncEditorBtn;

    @FXML
    private Button createDevBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applyBtn.disableProperty().bind(
                Bindings.or(
                        modInfoNameField.textProperty().isEmpty(),
                        modInfoAuthorField.textProperty().isEmpty()
                )
        );

        modInfoGrid.disableProperty().bind(
                Bindings.or(
                        viewDataModel.modProfilesDisableProperty(),
                        viewDataModel.modProfilesTable().selectionModelProperty().get().selectedItemProperty().isNull()
                )
        );
        modInfoBottomPane.disableProperty().bind(modInfoGrid.disableProperty());

        syncEditorBtn.disableProperty().bind(modInfoBottomPane.disabledProperty());
        createDevBtn.disableProperty().bind(modInfoBottomPane.disabledProperty());

        modInfoNameField.textProperty().bindBidirectional(viewDataModel.modNameProperty());
        modInfoAuthorField.textProperty().bindBidirectional(viewDataModel.modAuthorProperty());
        modDescriptionArea.textProperty().bindBidirectional(viewDataModel.modDescriptionProperty());
        modThumbnailField.textProperty().bindBidirectional(viewDataModel.modThumbnailProperty());

        modTypeBtn.textProperty().bindBidirectional(viewDataModel.modTypeProperty());
        modTypeBtn.textProperty().setValue("Mod Type");
    }

    @FXML
    private void onClickDependenciesBtn() throws IOException {
        stageManager.openView(ApplicationView.MOD_DEPENDENCIES);
    }

    @FXML
    private void onClickChooseModImageBtn() {
        File image = FileSelector.selectFile(SelectorsConstants.SELECT_MOD_IMAGE_TITLE,
                applicationConfig.getGameFolderPath(),
                new FileChooser.ExtensionFilter(SelectorsConstants.SELECT_MOD_IMAGE_DESCRIPTION, "*.png", "*.jpg"));

        if (image != null) {
            Mod mod = modProfilesManager.getSelectedMod();
            mod.setModImage(image);

            modThumbnailField.setText(image.getName());
        }
    }

    @FXML
    private void onClickSyncEditorBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();
        EditorSync.syncTo(selectedMod);
    }

    @FXML
    private void onClickCreateDevBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();
        selectedMod.generateDevFile();
    }

    @FXML
    private void onSelectModTypeMenu(Event event) {
         MenuItem item = (MenuItem) event.getSource();
         modTypeBtn.textProperty().setValue(item.getText());
    }

    @FXML
    private void onClickModInfoApplyBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();

        ModType type = Enum.valueOf(ModType.class, modTypeBtn.textProperty().getValue().toUpperCase());
        String name = modInfoNameField.getText();
        String author = modInfoAuthorField.getText();
        String description = modDescriptionArea.getText();
        String image = modThumbnailField.getText();
        String uid = selectedMod.getUID();
        String dependencyNames = selectedMod.getDependencyNames();

        // Update mod object
        Mod modFromManager = modProfilesManager.getModByPath(selectedMod.getLocation());
        modFromManager.setModName(name);
        modFromManager.setModAuthor(author);
        modFromManager.setDescription(description);
        modFromManager.setModImage(new File(String.format("%s/%s", selectedMod.getLocation().toString(), image)));
        modFromManager.setModType(type);

        // Update .hpc file
        XmlDocument modEntry;

        File modFolder = selectedMod.getLocation().toFile();

        if (!modFolder.getParentFile().exists()) {
            if (modFolder.getParentFile().mkdir()) {
                modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));

                modEntry.setRootElement("Content");
                modEntry.setRootAttribute("Version", "1.0.0");
                modEntry.setRootAttribute("InitCfg", "config/main_init.cfg");
            } else {
                AlertMaker.error(DialogConstants.DIALOG_MOD_APPLY_ERROR_TITLE,
                        DialogConstants.DIALOG_MOD_APPLY_ERROR_HEADER,
                        DialogConstants.DIALOG_MOD_APPLY_ERROR_CONTENT);

                return;
            }
        } else {
            modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));
        }

        modEntry.setRootAttribute("Type", type.getTypeName());
        modEntry.setRootAttribute("Title", name);
        modEntry.setRootAttribute("Author", author);
        modEntry.setRootAttribute("Description_english", description);
        modEntry.setRootAttribute("LauncherPic", image);
        modEntry.setRootAttribute("UID", uid);
        modEntry.setRootAttribute("Dependencies", dependencyNames);
        modEntry.save();

        // Update table
        viewDataModel.modProfilesTable().refresh();
    }
}
