package org.fg.rebirth.manager.util;

import org.fg.rebirth.manager.models.config.WipModConfig;
import org.fg.rebirth.manager.models.mod.Mod;

import javax.swing.*;
import java.io.File;

/**
 * Utility class for handling the WIPMod configuration of the level editor.
 */
public final class EditorSync {
    private EditorSync() {
    }

    /**
     * Sync a mod with the level editor. That will make the level editor load any custom assets that may be present
     * within a mod.
     *
     * @param mod The mod to sync the editor to.
     */
    public static void syncTo(Mod mod) {
        String myDocumentsDir = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        String engineDocumentsDir = myDocumentsDir + File.separator + "HPL3";

        String modEntryFilePath = mod.getLocation().toString();
        String configFilePath = engineDocumentsDir + File.separator + "WIPmod.cfg";

        WipModConfig configFile = new WipModConfig(mod, configFilePath);
        configFile.setModPath(modEntryFilePath);
    }
}
