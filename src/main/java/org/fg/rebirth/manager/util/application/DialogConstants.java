package org.fg.rebirth.manager.util.application;

import static org.fg.rebirth.manager.util.application.GlobalConstants.APPLICATION_REPOSITORY_URL;

public final class DialogConstants {
    private DialogConstants() {
    }

    //========================================DIALOG==============================================

    public static final String DIALOG_MOD_ADD_INFO_TITLE = "Add mod";
    public static final String DIALOG_MOD_ADD_INFO_HEADER = "Mod has already been added.";
    public static final String DIALOG_MOD_ADD_INFO_CONTENT = "";

    public static final String DIALOG_MOD_APPLY_ERROR_TITLE = "Apply mod changes";
    public static final String DIALOG_MOD_APPLY_ERROR_HEADER = "Failed to save mod";
    public static final String DIALOG_MOD_APPLY_ERROR_CONTENT = "Did you remove the mod or change its location?\n" +
            "Your game folder must include a 'mods' folder.";

    public static final String DIALOG_ABOUT_TITLE = "About Amnesia: Rebirth Mod Manager";
    public static final String DIALOG_ABOUT_HEADER = "Amnesia: Rebirth Mod Manager\n" +
            "\n" +
            "Version: 1.0.0\n" +
            "Made by TiMan";
    public static final String DIALOG_ABOUT_CONTENT = "This is a utility application for Amnesia: Rebirth.\n" +
            "Manage your mods and configure them with ease.\n" +
            "This project is open source and open for contribution.";

    public static final String DIALOG_REPO_TITLE = "View On GitLab";
    public static final String DIALOG_REPO_HEADER = "Could not open web browser";
    public static final String DIALOG_REPO_CONTENT = "Please visit" + APPLICATION_REPOSITORY_URL + " to view the repository.";

    public static final String DIALOG_BATCH_TITLE = "Create batch file";
    public static final String DIALOG_BATCH_PASSED_HEADER = "File created successfully.";
    public static final String DIALOG_BATCH_PASSED_CONTENT = "";
    public static final String DIALOG_BATCH_ERROR_HEADER = "Could not create mod batch file.";

    public static final String DIALOG_CONFIG_APP_ERROR_TITLE = "Create config file";
    public static final String DIALOG_CONFIG_APP_ERROR_HEADER = "Error creating config file";
    public static final String DIALOG_CONFIG_APP_ERROR_CONTENT = "Failed to create configuration file for the application.";

    public static final String DIALOG_CONFIG_WIP_ERROR_TITLE = "Create WipMod file";
    public static final String DIALOG_CONFIG_WIP_ERROR_HEADER = "Error creating WIPMod config file";
    public static final String DIALOG_CONFIG_WIP_ERROR_CONTENT = "Could not find HPL3 Editor configuration folder!";

    public static final String DIALOG_SYNC_PASSED_TITLE = "Sync editor";
    public static final String DIALOG_SYNC_PASSED_HEADER = "Mod successfully synced to level editor.";
    public static final String DIALOG_SYNC_PASSED_CONTENT = "";
}
