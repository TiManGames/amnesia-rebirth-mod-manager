package org.fg.rebirth.manager.util.javafx;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import org.fg.rebirth.manager.config.ApplicationTheme;

import java.util.Optional;

/**
 * Utility class for handling JavaFX alert windows.
 */
public final class AlertMaker {
    private AlertMaker() {
    }

    /**
     * Create a basic JavaFX error alert.
     *
     * @param title   Title of the alert.
     * @param header  Header of the alert.
     * @param content Content of the alert.
     *
     * @return True if clicked and closed.
     */
    public static boolean error(String title, String header, String content) {
        return createAlert(title, header, content, AlertType.ERROR);
    }

    /**
     * Create a basic JavaFX info alert.
     *
     * @param title   Title of the alert.
     * @param header  Header of the alert.
     * @param content Content of the alert.
     *
     * @return True if clicked and closed.
     */
    public static boolean info(String title, String header, String content) {
        return createAlert(title, header, content, AlertType.INFORMATION);
    }

    /**
     * Create a basic JavaFX confirm alert.
     *
     * @param title   Title of the alert.
     * @param header  Header of the alert.
     * @param content Content of the alert.
     *
     * @return True if clicked and closed.
     */
    public static boolean confirm(String title, String header, String content) {
        return createAlert(title, header, content, AlertType.CONFIRMATION);
    }

    private static boolean createAlert(String title, String header, String content, AlertType type) {
        Alert alert = new Alert(type);
        alert.getDialogPane().getStylesheets().add(ApplicationTheme.selectedTheme().getCss());
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        Optional<ButtonType> result = alert.showAndWait();

        return result.isPresent() && result.get() == ButtonType.OK && type == AlertType.CONFIRMATION;
    }
}
