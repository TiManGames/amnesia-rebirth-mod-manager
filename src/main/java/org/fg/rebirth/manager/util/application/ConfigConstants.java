package org.fg.rebirth.manager.util.application;

public final class ConfigConstants {
    private ConfigConstants() {
    }

    //========================================CONFIG==============================================

    public static final String CONFIG_APP_ELEMENT_GAME_DIR = "GameFolder";
    public static final String CONFIG_APP_ATTR_MOD_LOCATION = "Location";
}
