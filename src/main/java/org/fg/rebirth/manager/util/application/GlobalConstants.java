package org.fg.rebirth.manager.util.application;

public final class GlobalConstants {
    private GlobalConstants() {
    }

    //========================================GLOBALS=============================================

    public static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static final String USER_HOME = System.getProperty("user.home");

    public static final String APPLICATION_TITLE_MAIN = "Amnesia: Rebirth Mod Manager";
    public static final String APPLICATION_TITLE_DEP = "Manage Mod Dependencies";
    public static final String APPLICATION_REPOSITORY_URL = "https://gitlab.com/TiManGames/amnesia-rebirth-mod-manager";
}
