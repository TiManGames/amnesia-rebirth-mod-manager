package org.fg.rebirth.manager.models.config;

import org.fg.rebirth.manager.util.XmlDocument;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Base class for handling any configuration file in the form of xml file.
 */
public abstract class ConfigFile {
    protected final String configFilePath;
    protected XmlDocument xmlDocument;

    /**
     * Creates a config file in a specified path.<br>
     * If the file already exists, the xml document will parse the file data.
     *
     * @param pathname The specified path for the config file.
     */
    protected ConfigFile(String pathname) {
        this.configFilePath = pathname;

        createNewConfigFileIfNeeded();
    }

    /**
     * Generates a new config file document (.cfg).
     */
    protected abstract void generate();

    /**
     * Checks if the config file physically exists.
     *
     * @return True if the config file exists.
     */
    protected boolean isConfigFileExist() {
        return Files.exists(Paths.get(configFilePath));
    }

    /**
     * Creates a new config file if there isn't one existing already.
     */
    private void createNewConfigFileIfNeeded() {
        if (!isConfigFileExist()) {
            generate();
        }

        this.xmlDocument = new XmlDocument(new File(configFilePath));
    }
}
