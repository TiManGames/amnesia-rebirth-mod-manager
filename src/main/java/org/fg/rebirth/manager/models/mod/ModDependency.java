package org.fg.rebirth.manager.models.mod;

public final class ModDependency {
    private String dependencyName;

    public ModDependency(String dependencyName) {
        this.dependencyName = dependencyName;
    }

    public void setDependencyName(String dependencyName) {
        this.dependencyName = dependencyName;
    }

    public String getDependencyName() {
        return dependencyName;
    }
}
