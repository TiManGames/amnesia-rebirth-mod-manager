package org.fg.rebirth.manager.models.config;

import org.fg.rebirth.manager.models.mod.Mod;
import org.fg.rebirth.manager.util.XmlDocument;
import org.fg.rebirth.manager.util.application.DialogConstants;
import org.fg.rebirth.manager.util.javafx.AlertMaker;

import java.io.File;

/**
 * Configuration file (.cfg) for the Level Editor WIPMod configuration.
 */
public final class WipModConfig extends ConfigFile {
    private final String modEntryFilePath;

    /**
     * Creates a WIPMod config file.
     *
     * @param mod      The mod to set as a WIP Mod.
     * @param filePath The file path of the config file.
     */
    public WipModConfig(Mod mod, String filePath) {
        super(filePath);

        this.modEntryFilePath = String.format("%s/entry.hpc", mod.getLocation().toString());
    }

    /**
     * Sets a Mod as WIP Mod.
     *
     * @param pathname The path of the mod to set.
     */
    public void setModPath(String pathname) {
        if (isConfigFileExist()) {
            xmlDocument.setRootAttribute("Path", pathname);
            xmlDocument.save();

            AlertMaker.info(DialogConstants.DIALOG_SYNC_PASSED_TITLE,
                    DialogConstants.DIALOG_SYNC_PASSED_HEADER,
                    DialogConstants.DIALOG_SYNC_PASSED_CONTENT);
        }
    }

    @Override
    protected void generate() {
        xmlDocument = new XmlDocument(new File(super.configFilePath));
        xmlDocument.setRootElement("WIPmod");
        xmlDocument.setRootAttribute("Path", modEntryFilePath);
        xmlDocument.save();

        if (!isConfigFileExist()) {
            AlertMaker.error(DialogConstants.DIALOG_CONFIG_WIP_ERROR_TITLE,
                    DialogConstants.DIALOG_CONFIG_WIP_ERROR_HEADER,
                    DialogConstants.DIALOG_CONFIG_WIP_ERROR_CONTENT);
        }
    }
}
